import Vue from 'vue'
import VueRouter from 'vue-router'
import VueCompositionApi from '@vue/composition-api'
import VueService from '@/service'
import UIkit from 'uikit'
import Icons from 'uikit/dist/js/uikit-icons'
import Styles from 'uikit/src/less/uikit.less'
import router from '@/router'

Vue.config.productionTip = false
Vue.use(VueCompositionApi)
Vue.use(VueRouter)
Vue.use(VueService)

UIkit.use(Icons) && Styles

new Vue({
  router,
  render: h => <router-view/>
}).$mount('#app')
