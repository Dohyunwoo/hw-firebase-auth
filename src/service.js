// importing @/services/*.js, BEWARE of test.js
const req = require.context('@/services', true, /.js$/);
const registry = Object.fromEntries( // expect entries below
  req.keys().map(path => [                   // [name, module] entry
    path.match(/\.\/(?<name>[\w-]+)\.js/).groups.name, req(path)
  ])
);
// registry will look like this {
//   exactearth: Module,
//   firebase: Module,
//   mapbox: Module,
//   noop: Module,
//   ...: ...,
// }
export default new class VueService {
  install(Vue, options) {
    Object.values(registry).forEach(module => {
      if (module.default && module.default.install) {
        module.default.install(Vue, options);
      }
    });
    Vue.directive('serve', hook);
    Vue.prototype.$registry = registry;
  }
}
const hook = Object.fromEntries( // expect entries below
  'bind inserted update componentUpdated unbind'.split(' ').map(name => [
    name, function(el, { arg }) {
      const f = registry[arg].default[name];
      f && f(...arguments);
    }
  ])
);
