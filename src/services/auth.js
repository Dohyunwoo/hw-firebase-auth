import FirebaseService from '@/services/firebase'

const auth = FirebaseService.apps.default.auth

const createUser = (email, password) => {
  const prom = auth.createUserWithEmailAndPassword(email,password)
  return prom.then(res => {
    alert(`${res.user.email} 님 회원가입 완료`)
  }).catch(err => {
    console.log(err.code)
    alert(err.message)
  })
}

export default createUser
