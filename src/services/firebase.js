import Vue from 'vue'
import VueCompositionApi from '@vue/composition-api'

// if a service want to provide reactivity,
// 1. import like below as react
import { reactive as react } from '@vue/composition-api';

Vue.use(VueCompositionApi)

// we have a global firebase reference already initialized
// so we use that to setup then dispose from the window name space
console.log('service:firebase');

// 2. and then export reactive instance like this
// a service should only export one reactive instance for less confusing.
export const reactive = react({
  user: null, // add more computed props...
});

// 3. now other codes can update the reactive instance at any level
class Firebase {
  static App = a => new this(a);
  constructor(app) {
    this.app = app;
    this.auth = app.auth();
    this.auth.useDeviceLanguage();
    this.auth.onAuthStateChanged(user => {
      reactive.user = user;
    });
    this.firestore = app.firestore();
  }
  // vm.$firebase.fetched can wait for the first resolve at the auth state
  get fetched() {
    return new Promise((resolve, reject) => {
      if (reactive.user) return resolve(reactive.user);
      const off = this.auth.onAuthStateChanged(user => {
        off(); resolve(user);
      }, reject);
    });
  }
}

export default new class FirebaseService {
  apps = { default: Firebase.App(firebase.app()) };
  install(Vue, options) { // interface method, service.js will call this
    Vue.prototype.$firebase = this.apps.default;
  }
}
window.firebase = undefined
window.firebaseConfig = undefined

// 4. and what kind of way to import this `reactive` would be nice?
//
// <script>
// // import like below
// // so that we can know we are importing a reactive instance.
// // and give an explicit alias to make it self-explanatory
//
// import { reactive as auth } from '@/services/firebase';
// export default {
//   setup() {
//     return { auth };
//   }
// }
// </script>
// <template>
//   <section>{{auth.user}}</section>
// </template>
